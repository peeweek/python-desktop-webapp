# python-desktop-webapp

Template for starting projects using python, `Flask`, and `webview`, and ability to deploy as executable using `pyinstaller` 

## Requirements

- A Linux distribution (for now, Windows and mac are not (yet)* supported)
- `cmake` for makefile*
- `python` 3.12 *
  - `virtualenv` required for system python
  - Dependencies : these are bundled in `requirements.txt` and automatically installed when using make
- System Dependencies for `pywebview` : https://pywebview.flowrl.com/guide/installation.html#dependencies
  - GTK: `python3-gi` `python3-gi-cairo` `gir1.2-gtk-3.0` `gir1.2-webkit2-4.1`

(*) For python and make installation see your operating system's packaging requirements. 

## Installation and Usage
After cloning the project, and install of cmake and python navigate to the project folder and use:

- `make prepare` to prepare the virtual environment
- `make run` to run the app
- `make install` to build a standalone executable 

## How it works

The app starts 2 components : 

- A Flask Application that runs as a WSGI server on the localhost, this app serves the pages that will be displayed and used as a GUI for the application
- A webview (simple window) that displays the pages of the Flask application

The webview application can be configured using the `config.yml` yaml File

## License

``` 
MIT License

Copyright (c) 2024 Thomas Iché

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

