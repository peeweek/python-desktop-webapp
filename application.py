import webview, yaml
from flask import Flask, Response, request, render_template

config = None
with open("config.yml") as config_file:
    config = yaml.safe_load(config_file)

app = Flask(__name__, static_folder='./static', template_folder='./templates')

def template(name: str):
    return render_template("{}.template.html".format(name))

@app.route('/')
def main():
    return template("index")

@app.route('/<route>')
def route(route):
    return template(route)

@app.route('/query=<query>')
def query(query):
    return("Received a query of name '{}'".format(query))

if __name__ == '__main__':
    window = webview.create_window(config["title"], 
        app, 
        resizable=config["resizable"],
        fullscreen=config["fullscreen"],
        min_size=(config["min_width"],config["min_height"]),
        width=config["width"],
        height=config["height"],
        x=config["position_x"],
        y=config["position_y"],
        frameless=config["frameless"],
        easy_drag=config["easy_drag"],
        transparent=config["transparent"],
        on_top=config["on_top"],
        )
    webview.start(debug=config["debug"])

