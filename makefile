prepare:
	@echo "Removing old VirtualEnv..."
	@rm -rf .venv
	@echo "Creating new VirtualEnv in .venv..."
	@python -m venv .venv
	@echo "Upgrading PIP..."
	@.venv/bin/python -m pip install --upgrade pip
	@echo "Installing Requirements..."
	@.venv/bin/python -m pip install -r ./requirements.txt
	@echo "Prepare complete ! You can now run 'make run' or 'make install'"

install:
	@echo "Cleaning up.."
	@rm -rf build
	@rm -rf dist
	@echo "Building executable"
	@.venv/bin/pyinstaller -w -F --add-data "templates:templates" --add-data "static:static" application.py
	@echo "Creating symlink"
	@ln -s ./dist/application ./application
	@echo "Build Complete! run ./application to start built executable"

package:
	@echo "Building Package..."
	@zip -r ./desktop-webapp-package.zip ./static ./templates ./makefile ./application.py ./application.spec ./config.yml ./requirements.txt
	@echo "Done ! Written desktop-webapp-package.zip"

run:
	@.venv/bin/python application.py
